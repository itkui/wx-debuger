/*
 * @Author: ItKui<13452903663@163.com>
 * @Date: 2020-10-16 08:58:27
 * @LastEditTime: 2020-10-16 15:25:13
 * @FilePath: \wx-debuger\routes\index.js
 * @FileName: 
 */
var router = require('koa-router')();

router.get('/', function *(next) {
  console.log(this.session)
  yield this.render('index', {
    title: 'Hello World Koa!'
  });
});

module.exports = router;
