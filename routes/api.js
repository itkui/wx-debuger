/*
 * @Author: ItKui<13452903663@163.com>
 * @Date: 2020-10-16 08:58:27
 * @LastEditTime: 2020-10-16 15:10:38
 * @FilePath: \wx-debuger\routes\api.js
 * @FileName:
 */
var router = require("koa-router")();
const Wx = require("./../controllers/wx");

router.prefix("/api");
router.post("/sign", function* (next) {
  yield Wx.getSign(this);
});

module.exports = router;
